'use strict'

function load(selector) {
    const data = init(selector)

    data.loop = () => {
        update(data);
        render(data);
        requestAnimationFrame(data.loop);
    };

    addEvents(data)

    data.loop();
}

function init(selector) {
    const data = {}

    data.ctx = document.querySelector(selector).getContext('2d')
    data.points = []
    data.connections = []

    return data
}

function update(data) { }

function render(data) {
    const { ctx, points, connections } = data

    ctx.fillStyle = '#000'
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)

    ctx.strokeStyle = '#ff0'
    connections.forEach(function (connection) {
        ctx.beginPath()
        ctx.moveTo(connection.from.x, connection.from.y)
        ctx.lineTo(connection.to.x, connection.to.y)
        ctx.stroke()
    })

    ctx.fillStyle = '#fff'

    points.forEach(function (point) {
        ctx.fillRect(point.x - 5, point.y - 5, 10, 10)
    })
}

function addEvents(data) {
    const canvas = data.ctx.canvas

    canvas.addEventListener('click', function (event) {
        data.points.push(getRelativeCoordinates(canvas, event))
        triangulate(data)
    })
}

function getRelativeCoordinates(element, event) {
    return {
        x: event.clientX - element.offsetLeft,
        y: event.clientY - element.offsetTop
    }
}

function triangulate(data) {
    const { points } = data

    const orderedPoints = [...new Set(points)].sort(function (a, b) {
        return a.x == b.x ? a.y - b.y : a.x - b.x
    })

    data.connections = _triangulate(orderedPoints)
}

function _triangulate(points, connections = []) {
    if (points.length < 4) {
        for (let i = 0; i < points.length; i++) {
            for (let j = i + 1; j < points.length; j++) {
                connections.push(_getConnection(points[i], points[j]))
            }
        }

        return connections
    }

    const pivot = (points.length / 2) >> 0

    const leftPoints = points.slice(0, pivot)
    const rightPoints = points.slice(pivot, points.length)

    const leftConnections = _triangulate(leftPoints)
    const rightConnections = _triangulate(rightPoints)

    const middleConnections = _connectTriangles(leftPoints, rightPoints)

    return leftConnections.concat(rightConnections).concat(middleConnections)
}

function _getConnection(a, b) {
    return {
        from: a,
        to: b
    }
}

function _connectTriangles(pointsLeft, pointsRight) {
    const connections = []
    let currentLeft = _getHighest(pointsLeft)
    let currentRight = _getHighest(pointsRight)

    connections.push({
        from: currentLeft,
        to: currentRight
    })

    return connections
}

function _getHighest(points) {
    return points.sort(function (a, b) {
        return a.y - b.y
    })[0]
}
